#!/usr/bin/env bash

# This file deploys the containerised app to my local Vagrant env

declare -a files=("Dockerfile" "package.json" "package-lock.json" "server.js")

mkdir -p /c/Projects/moe-wfiam/platform/nodeApp

for f in "${files[@]}"; do
    echo "Copying file ${f} to volume"
    chmod u+x "${f}"
    cp "${f}" /c/Projects/moe-wfiam/platform/nodeApp/
done

# Docker 
pushd /c/Projects/moe-wfiam/platform

# Do this to set up the user in Vagrant
# Add vagrant user to docker group
# sudo groupadd docker
# sudo usermod -aG docker $USER
# newgrp docker
# docker run hello-world

vagrant ssh << EOF
docker container kill node-app-one
docker build -t node-app /vagrant/nodeApp/
docker run -p 8081:3000 --name node-app-one --rm -d node-app
EOF

popd