const request = require('supertest');
const app = require('../server');

describe('Get endpoints', () => {
  it('should return a string', async () => {
    const res = await request(app).get('/');

    expect(res.status).toEqual(200);
    expect(res.body.message).toEqual('Hello John');
  });
});
